FROM centos:7
LABEL author="Mikael Karlsson <i8myshoes@gmail.com>"
WORKDIR /root
ARG GHC_DW

RUN yum clean all; yum -y install epel-release deltarpm; yum clean all; yum -y upgrade
ADD https://packages.irods.org/renci-irods.yum.repo /etc/yum.repos.d/irods.repo
ADD https://download.opensuse.org/repositories/shells:fish:release:2/CentOS_7/shells:fish:release:2.repo /etc/yum.repos.d/fish.repo

RUN yum -y install gcc-c++ zlib-devel xz-devel ncurses-devel irods-devel irods-runtime binutils elfutils-devel openssl-devel numactl-devel libzstd-devel lz4-devel valgrind file htop fish

RUN curl -sSL https://get.haskellstack.org/ | sh
RUN curl -s https://www.haskell.org/cabal/release/cabal-install-2.2.0.0/cabal-install-2.2.0.0-x86_64-unknown-linux.tar.gz | tar -xkzC /usr/local/bin --no-same-owner
RUN curl -s https://releases.llvm.org/5.0.2/clang+llvm-5.0.2-x86_64-linux-gnu-ubuntu-14.04.tar.xz | tar -xkJC /usr/local --strip-components 1 --no-same-owner
RUN curl -s https://downloads.haskell.org/~ghc/8.4.4/ghc-8.4.4-x86_64-deb9-linux${GHC_DW}.tar.xz | tar -xkJ --no-same-owner && (cd ghc* && CC=clang ./configure && make install) && rm -rf ghc*
RUN (cd /usr/local/bin && curl -sL https://github.com/sol/hpack/releases/download/0.31.0/hpack_linux.gz | gunzip > hpack && chmod 555 hpack)

RUN yum clean all; rm -rf /var/cache/yum

RUN useradd ghc
USER ghc
WORKDIR /home/ghc
