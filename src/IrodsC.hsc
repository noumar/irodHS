{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MagicHash #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module IrodsC
(
  columnMetadataDataAttributeName,
  columnMetadataDataAttributeValue,
  dataTypeKW,
  destinationResourceNameKW,
  filePathKW,
  forceFlagKW,
  longMetadata,
  orderBy,
  readOnly,
  readWrite,
  recursiveOperationKW,
  recursiveQuery,
  registerChecksumKW,
  replicaNumKW,
  seekCur,
  seekEnd,
  seekSet,
  verifyChecksumKW,
  veryLongMetadata,
  writeOnly,
  BytesBuffer(..),
  CollectionEntry,
  CollectionInput(..),
  DataObject(..),
  ErrorMsg(..),
  IndexIntValuePairs(..),
  IndexValuePairs(..),
  KeywordValuePairs(..),
  ObjectStat,
  OpenObject(..),
  OpenStat,
  QueryInput(..),
  QueryOutput(attributeCount, sqlResults),
  SqlResult(unSqlResult),
  PamRequest,
  PamResponse,
)
where

import Data.Aeson (ToJSON)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Unsafe as BSU (unsafeUseAsCString, unsafePackMallocCString, unsafePackAddress)
import Data.Coerce (coerce)
import qualified Data.Text as T
import Foreign
import Foreign.C.String
import Foreign.C.Types
import GHC.Generics (Generic)
import Prelude
import System.IO.Unsafe (unsafePerformIO)
import System.Posix.Internals (sEEK_CUR, sEEK_END, sEEK_SET, o_RDONLY, o_RDWR, o_WRONLY)
import Data.Time.Clock.POSIX (POSIXTime)
import Text.Read (readMaybe)

#include <irods/dataObjInpOut.h>
#include <irods/rodsError.h>

newtype Seek = Seek CInt
  deriving stock (Show)
  deriving newtype (Storable)

seekCur = Seek sEEK_CUR
seekEnd = Seek sEEK_END
seekSet = Seek sEEK_SET

newtype OpenFlag = OpenFlag CInt
  deriving stock (Show)
  deriving newtype (Storable)

readOnly = OpenFlag o_RDONLY
readWrite = OpenFlag o_RDWR
writeOnly = OpenFlag o_WRONLY

type RodsLong = Int

-- https://github.com/irods/irods/blob/master/lib/core/include/rodsError.h#L19
data {-# CTYPE "rErrMsg_t" #-} ErrorMsg = ErrorMsg
  { status :: !CInt
  , msg :: T.Text
  }
  deriving stock (Show)

instance Storable ErrorMsg where
  sizeOf _ = #{size rErrMsg_t}
  alignment _ = #{alignment rErrMsg_t}
  peek ptr = do
    status <- #{peek rErrMsg_t, status} ptr
    msg <- (pure . T.pack) =<< peekCString (#{ptr rErrMsg_t, msg} ptr)
    return ErrorMsg {..}

-- https://github.com/irods/irods/blob/master/lib/api/include/dataObjInpOut.h#L65
data {-# CTYPE "dataObjInp_t" #-} DataObject = DataObject
  { objectPath :: T.Text
  , createMode :: !CInt
  , openFlags :: !OpenFlag
  , dataSize :: !RodsLong
  , numThreads :: !CInt
  , conditions :: !KeywordValuePairs
  }
  deriving stock (Show)

-- https://github.com/irods/irods/blob/master/lib/api/include/dataObjInpOut.h#L65
instance Storable DataObject where
  sizeOf _ = (#size dataObjInp_t)
  alignment _ = (#alignment dataObjInp_t)
  peek ptr = do
    objectPath <- (pure . T.pack) =<< peekCString (#{ptr dataObjInp_t, objPath} ptr)
    createMode <- (#peek dataObjInp_t, createMode) ptr
    openFlags <- (#peek dataObjInp_t, openFlags) ptr
    dataSize <- (#peek dataObjInp_t, dataSize) ptr
    numThreads <- (#peek dataObjInp_t, numThreads) ptr
    conditions <- (#peek dataObjInp_t, condInput) ptr
    return DataObject {..}
  poke ptr DataObject {..} = do
    fillBytes ptr 0 (#size dataObjInp_t)
    withCString (T.unpack objectPath) $ \cString -> do
      cArray <- peekArray0 0 cString
      pokeArray ((#ptr dataObjInp_t, objPath) ptr) cArray
    (#poke dataObjInp_t, createMode) ptr createMode
    (#poke dataObjInp_t, openFlags) ptr openFlags
    (#poke dataObjInp_t, dataSize) ptr dataSize
    (#poke dataObjInp_t, numThreads) ptr numThreads
    (#poke dataObjInp_t, condInput) ptr conditions

newtype {-# CTYPE "bytesBuf_t" #-} BytesBuffer = BytesBuffer CStringLen

instance Storable BytesBuffer where
  sizeOf _ = (#size bytesBuf_t)
  alignment _ = (#alignment bytesBuf_t)
  peek ptr = do
    buf <- (#peek bytesBuf_t, buf) ptr
    len <- (#peek bytesBuf_t, len) ptr
    return (BytesBuffer (buf, len))
  poke ptr (BytesBuffer (buf, len)) = do
    fillBytes ptr 0 (#size bytesBuf_t)
    (#poke bytesBuf_t, buf) ptr buf
    (#poke bytesBuf_t, len) ptr len

data {-# CTYPE "openedDataObjInp_t" #-} OpenObject = OpenObject
  { l1descInx :: !CInt
  , len :: !CInt
  , whence :: !Seek
  , conditions :: !KeywordValuePairs
  }
  deriving stock (Show)

-- https://github.com/irods/irods/blob/master/lib/api/include/dataObjInpOut.h#L104
instance Storable OpenObject where
  sizeOf _ = (#size openedDataObjInp_t)
  alignment _ = (#alignment openedDataObjInp_t)
  peek ptr = do
    l1descInx <- (#peek openedDataObjInp_t, l1descInx) ptr
    len <- (#peek openedDataObjInp_t, len) ptr
    whence <- (#peek openedDataObjInp_t, whence) ptr
    conditions <- (#peek openedDataObjInp_t, condInput) ptr
    return OpenObject {..}
  poke ptr OpenObject {..} = do
    fillBytes ptr 0 (#size openedDataObjInp_t)
    (#poke openedDataObjInp_t, l1descInx) ptr l1descInx
    (#poke openedDataObjInp_t, len) ptr len
    (#poke openedDataObjInp_t, whence) ptr whence
    (#poke openedDataObjInp_t, condInput) ptr conditions

newtype CollectionFlag = CollectionFlag CInt
  deriving newtype (Bits, Eq, Num, Storable)

#{enum CollectionFlag, CollectionFlag
 , longMetadata = LONG_METADATA_FG
 , veryLongMetadata = VERY_LONG_METADATA_FG
 , recursiveQuery = RECUR_QUERY_FG
 , dataQueryFirst = DATA_QUERY_FIRST_FG
 , includeConditionalInput = INCLUDE_CONDINPUT_IN_QUERY
 }

-- https://github.com/irods/irods/blob/master/lib/api/include/dataObjInpOut.h#L157
data {-# CTYPE "collInp_t" #-} CollectionInput = CollectionInput
  { collectionName :: T.Text
  , flags :: [CollectionFlag]
  , conditions :: !KeywordValuePairs
  }

instance Storable CollectionInput where
  sizeOf _ = #{size collInp_t}
  alignment _ = #{alignment collInp_t}
  poke ptr CollectionInput {..} = do
    fillBytes ptr 0 #{size collInp_t}
    withCString (T.unpack collectionName) $ \cString -> do
      cArray <- peekArray0 0 cString
      pokeArray (#{ptr collInp_t, collName} ptr) cArray
    #{poke collInp_t, flags} ptr (foldr (.|.) 0 flags)
    #{poke collInp_t, condInput} ptr conditions

-- https://github.com/irods/irods/blob/master/lib/core/include/miscUtil.h#L112
data {-# CTYPE "collEnt_t" #-} CollectionEntry = CollectionEntry
  { objectType :: !ObjectType
  , replicaNum :: !Int
  , replicaStatus :: !Int
  , dataMode :: !Int
  , dataSize :: !RodsLong
  , collectionName :: Maybe T.Text -- valid for dataObj and collection */
  , dataName :: Maybe T.Text
  , dataId :: Maybe Int
  , createTime :: Maybe POSIXTime
  , modifyTime :: Maybe POSIXTime
  , checksum :: Maybe T.Text
  , resource :: Maybe T.Text
  , resourceHierarchy :: Maybe T.Text
  , physicalPath :: Maybe T.Text
  , ownerName :: Maybe T.Text -- valid for dataObj and collection
  , dataType :: Maybe T.Text -- JMC - backport 4636
  }
  deriving stock (Generic, Show)
  deriving anyclass (ToJSON)

#include <irods/miscUtil.h>
-- https://github.com/irods/irods/blob/master/lib/core/include/miscUtil.h#L112
instance Storable CollectionEntry where
  sizeOf _ = #{size collEnt_t}
  alignment _ = #{alignment collEnt_t}
  peek ptr = do
    objectType <- #{peek collEnt_t, objType} ptr
    replicaNum <- fromIntegral <$> (#{peek collEnt_t, replNum} ptr :: IO CInt)
    replicaStatus <- fromIntegral <$> (#{peek collEnt_t, replStatus} ptr :: IO CInt)
    dataMode <- fromIntegral <$> (#{peek collEnt_t, dataMode} ptr :: IO CUInt)
    dataSize <- #{peek collEnt_t, dataSize} ptr
    collectionName <- toText <$> (maybePeek peekCString =<< #{peek collEnt_t, collName} ptr)
    dataName <- toText <$> (maybePeek peekCString =<< #{peek collEnt_t, dataName} ptr)
    dataId <- toNum <$> (maybePeek peekCString =<< #{peek collEnt_t, dataId} ptr)
    createTime <- toNum <$> (maybePeek peekCString =<< #{peek collEnt_t, createTime} ptr)
    modifyTime <- toNum <$> (maybePeek peekCString =<< #{peek collEnt_t, modifyTime} ptr)
    checksum <- toText <$> (maybePeek peekCString =<< #{peek collEnt_t, chksum} ptr)
    resource <- toText <$> (maybePeek peekCString =<< #{peek collEnt_t, resource} ptr)
    resourceHierarchy <- toText <$> (maybePeek peekCString =<< #{peek collEnt_t, resc_hier} ptr)
    physicalPath <- toText <$> (maybePeek peekCString =<< #{peek collEnt_t, phyPath} ptr)
    ownerName <- toText <$> (maybePeek peekCString =<< #{peek collEnt_t, ownerName} ptr)
    dataType <- toText <$> (maybePeek peekCString =<< #{peek collEnt_t, dataType} ptr)
    return CollectionEntry {..}

toNum ms = do
  s <- ms
  t <- readMaybe s
  return $ fromIntegral t

toText ms = do
  es <- ms
  case es of
    [] -> Nothing
    s -> Just $ T.pack s

-- https://github.com/irods/irods/blob/master/lib/core/include/rodsType.h#L36
data {-# CTYPE "objType_t" #-} ObjectType
  = UnknownObjectType
  | DataObjectType
  | CollectionObjectType
  | UnknownFileType
  | LocalFileType
  | LocalDirType
  | NoInputType
  deriving stock (Enum, Generic, Show)
  deriving anyclass (ToJSON)

instance Storable ObjectType where
  sizeOf _ = sizeOf (undefined :: CInt)
  alignment _ = alignment (undefined :: CInt)
  peek ptr = toEnum . fromIntegral <$> (peek (castPtr ptr) :: IO CInt)
  poke ptr v = poke (castPtr ptr) (fromIntegral (fromEnum v) :: CInt)

newtype Keyword = Keyword BS.ByteString
  deriving stock (Show)

#include <irods/rodsKeyWdDef.h>
-- https://github.com/irods/irods/blob/master/lib/core/include/rodsKeyWdDef.h
dataTypeKW = packKeyword #{const_str DATA_TYPE_KW}##
destinationResourceNameKW = packKeyword #{const_str DEST_RESC_NAME_KW}##
filePathKW = packKeyword #{const_str FILE_PATH_KW}##
forceFlagKW = packKeyword #{const_str FORCE_FLAG_KW}##
recursiveOperationKW = packKeyword #{const_str RECURSIVE_OPR__KW}##
registerChecksumKW = packKeyword #{const_str REG_CHKSUM_KW}##
replicaNumKW = packKeyword #{const_str REPL_NUM_KW}##
verifyChecksumKW = packKeyword #{const_str VERIFY_CHKSUM_KW}##

-- | Pack pinned literal string address into ByteString wrapped as Keyword
packKeyword kw = Keyword $ unsafePerformIO $ BSU.unsafePackAddress kw

newtype {-# CTYPE "keyValPair_t" #-} KeywordValuePairs = KeywordValuePairs [(Keyword, T.Text)]
  deriving stock (Show)

-- https://github.com/irods/irods/blob/master/lib/core/include/objInfo.h#L119
instance Storable KeywordValuePairs where
  sizeOf _ = (#size keyValPair_t)
  alignment _ = (#alignment keyValPair_t)
  peek ptr = do
    len <- (#peek keyValPair_t, len) ptr
    kw <- mapM BSU.unsafePackMallocCString =<< peekArray len =<< (#peek keyValPair_t, keyWord) ptr
    val <- mapM peekCString =<< peekArray len =<< (#peek keyValPair_t, value) ptr
    return $ KeywordValuePairs $ zip (coerce kw) (map T.pack val)
  poke ptr (KeywordValuePairs x) = do
    let (kw, val) = unzip x
    fillBytes ptr 0 (#size keyValPair_t)
    (#poke keyValPair_t, len) ptr (length x)
    (#poke keyValPair_t, keyWord) ptr =<< newArray =<< mapM (flip BSU.unsafeUseAsCString return) (coerce kw)
    (#poke keyValPair_t, value) ptr =<< newArray =<< mapM (newCString . T.unpack) val

-- https://github.com/irods/irods/blob/master/lib/core/include/rodsGenQuery.h#L24
data {-# CTYPE "genQueryInp_t" #-} QueryInput = QueryInput
  { maxRows :: !CInt
  , continueIndex :: !CInt
  , rowOffset :: !CInt
  , options :: [Option]
  , conditions :: !KeywordValuePairs
  , select :: !IndexIntValuePairs
  , sqlConditions :: !IndexValuePairs
  }

instance Storable QueryInput where
  sizeOf _ = #{size genQueryInp_t}
  alignment _ = #{alignment genQueryInp_t}
  poke ptr QueryInput {..} = do
    fillBytes ptr 0 #{size genQueryInp_t}
    #{poke genQueryInp_t, maxRows} ptr maxRows
    #{poke genQueryInp_t, continueInx} ptr continueIndex
    #{poke genQueryInp_t, rowOffset} ptr rowOffset
    #{poke genQueryInp_t, options} ptr (foldr (.|.) 0 options)
    #{poke genQueryInp_t, condInput} ptr conditions
    #{poke genQueryInp_t, selectInp} ptr select
    #{poke genQueryInp_t, sqlCondInp} ptr sqlConditions

-- https://github.com/irods/irods/blob/master/lib/core/include/rodsGenQuery.h#L67
data {-# CTYPE "genQueryOut_t" #-} QueryOutput = QueryOutput
  { rowCount :: !CInt
  , attributeCount :: !CInt
  , continueIndex :: !CInt
  , totalRowCount :: !CInt
  , sqlResults :: [SqlResult]
  }
  deriving stock (Show)

instance Storable QueryOutput where
  sizeOf _ = #{size genQueryOut_t}
  alignment _ = #{alignment genQueryOut_t}
  peek ptr = do
    rowCount <- #{peek genQueryOut_t, rowCnt} ptr
    attributeCount <- #{peek genQueryOut_t, attriCnt} ptr
    continueIndex <- #{peek genQueryOut_t, continueInx} ptr
    totalRowCount <- #{peek genQueryOut_t, totalRowCount} ptr
    sqlResults <- peekArray (fromIntegral attributeCount) $ #{ptr genQueryOut_t, sqlResult} ptr
    return QueryOutput {..}

-- https://github.com/irods/irods/blob/master/lib/core/include/rodsGenQuery.h#L61
newtype {-# CTYPE "sqlResult_t" #-} SqlResult = SqlResult {unSqlResult :: (Index, BS.ByteString)}
  deriving stock (Show)

instance Storable SqlResult where
  sizeOf _ = #{size sqlResult_t}
  alignment _ = #{alignment sqlResult_t}
  peek ptr = do
    attributeIndex <- #{peek sqlResult_t, attriInx} ptr
    value <- BS.packCString =<< #{peek sqlResult_t, value} ptr
    return $ SqlResult (MetadataColumn attributeIndex, value) -- TODO! Need to call right column constructor

-- https://github.com/irods/irods/blob/master/lib/core/include/objInfo.h#L205
newtype {-# CTYPE "inxIvalPair_t" #-} IndexIntValuePairs = IndexIntValuePairs [(Index, IntValue)]

instance Storable IndexIntValuePairs where
  sizeOf _ = #{size inxIvalPair_t}
  alignment _ = #{alignment inxIvalPair_t}
  poke ptr (IndexIntValuePairs x) = do
    let (index, val) = unzip x
    fillBytes ptr 0 #{size inxIvalPair_t}
    #{poke inxIvalPair_t, len} ptr (length x)
    #{poke inxIvalPair_t, inx} ptr =<< newArray (map unIndex index)
    #{poke inxIvalPair_t, value} ptr =<< newArray (map unIntValue val)

-- https://github.com/irods/irods/blob/master/lib/core/include/objInfo.h#L213
newtype {-# CTYPE "inxValPair_t" #-} IndexValuePairs = IndexValuePairs [(Index, T.Text)]

instance Storable IndexValuePairs where
  sizeOf _ = #{size inxValPair_t}
  alignment _ = #{alignment inxValPair_t}
  poke ptr (IndexValuePairs x) = do
    let (index, val) = unzip x
    fillBytes ptr 0 #{size inxValPair_t}
    #{poke inxValPair_t, len} ptr (length x)
    #{poke inxValPair_t, inx} ptr =<< newArray (map unIndex index)
    #{poke inxValPair_t, value} ptr =<< newArray =<< mapM (newCString . T.unpack) val

data IntValue
  = Order {unIntValue :: !CInt}
  | Operation {unIntValue :: !CInt}

newtype Option = Option CInt
  deriving newtype (Bits, Eq, Num, Storable)

data Index
  = ZoneColumn {unIndex :: !CInt}
  | UserColumn {unIndex :: !CInt}
  | ResourceColumn {unIndex :: !CInt}
  | DataColumn {unIndex :: !CInt}
  | CollectionColumn {unIndex :: !CInt}
  | MetadataColumn {unIndex :: !CInt}
  deriving stock (Show)

#include <irods/pamAuthRequest.h>

-- https://github.com/irods/irods/blob/master/lib/api/include/pamAuthRequest.h#L6
data {-# CTYPE "pamAuthRequestInp_t" #-} PamRequest = PamRequest
  { pamUser :: !BS.ByteString
  , pamPassword :: !BS.ByteString
  , timeToLive :: !CInt
  }

instance Storable PamRequest where
  sizeOf _ = #{size pamAuthRequestInp_t}
  alignment _ = #{alignment pamAuthRequestInp_t}
  poke ptr PamRequest {..} = do
    fillBytes ptr 0 #{size pamAuthRequestInp_t}
    BSU.unsafeUseAsCString pamUser $ #{poke pamAuthRequestInp_t, pamUser} ptr
    BSU.unsafeUseAsCString pamPassword $ #{poke pamAuthRequestInp_t, pamPassword} ptr
    #{poke pamAuthRequestInp_t, timeToLive} ptr timeToLive

-- https://github.com/irods/irods/blob/master/lib/api/include/pamAuthRequest.h#L14
newtype {-# CTYPE "pamAuthRequestOut_t" #-} PamResponse = PamResponse {irodsPamPassword :: Maybe BS.ByteString}
  deriving stock (Show)

instance Storable PamResponse where
  sizeOf _ = #{size pamAuthRequestOut_t}
  alignment _ = #{alignment pamAuthRequestOut_t}
  peek ptr = do
    irodsPamPassword <- maybePeek BSU.unsafePackMallocCString =<< #{peek pamAuthRequestOut_t, irodsPamPassword} ptr
    return PamResponse {..}

#include <irods/dataObjOpenAndStat.h>

-- https://github.com/irods/irods/blob/master/lib/api/include/dataObjOpenAndStat.h#L8
data {-# CTYPE "openStat_t" #-} OpenStat = OpenStat
  { dataSize :: !RodsLong
  , dataType :: Maybe T.Text
  , dataMode :: Maybe Int
  , l3descInx :: !CInt
  , replicaStatus :: !CInt
  , rescourceTypeIndex :: !CInt
  , replicaNum :: !CInt
  }
  deriving stock (Show)

instance Storable OpenStat where
  sizeOf _ = #{size openStat_t}
  alignment _ = #{alignment openStat_t}
  peek ptr = do
    dataSize <- #{peek openStat_t, dataSize} ptr
    dataType <- toText <$> maybePeek peekCString (#{ptr openStat_t, dataType} ptr)
    dataMode <- toNum <$> maybePeek peekCString (#{ptr openStat_t, dataMode} ptr)
    l3descInx <- #{peek openStat_t, l3descInx} ptr
    replicaStatus <- #{peek openStat_t, replStatus} ptr
    rescourceTypeIndex <- #{peek openStat_t, rescTypeInx} ptr
    replicaNum <- #{peek openStat_t, replNum} ptr
    return OpenStat {..}

data {-# CTYPE "rodsObjStat_t" #-} ObjectStat = ObjectStat
  { objectSize :: !RodsLong
  , objectType :: !ObjectType
  , dataMode :: !Int
  , dataId :: Maybe Int
  , checksum :: Maybe T.Text
  , ownerName :: Maybe T.Text
  , ownerZone :: Maybe T.Text
  , createTime :: Maybe POSIXTime
  , modifyTime :: Maybe POSIXTime
  , resourceHierarchy :: Maybe T.Text
  }
  deriving stock (Show)

instance Storable ObjectStat where
  sizeOf _ = #{size rodsObjStat_t}
  alignment _ = #{alignment rodsObjStat_t}
  peek ptr = do
    objectSize <- #{peek rodsObjStat_t, objSize} ptr
    objectType <- #{peek rodsObjStat_t, objType} ptr
    dataMode <- fromIntegral <$> (#{peek rodsObjStat_t, dataMode} ptr :: IO CUInt)
    dataId <- toNum <$> maybePeek peekCString (#{ptr rodsObjStat_t, dataId} ptr)
    checksum <- toText <$> maybePeek peekCString (#{ptr rodsObjStat_t, chksum} ptr)
    ownerName <- toText <$> maybePeek peekCString (#{ptr rodsObjStat_t, ownerName} ptr)
    ownerZone <- toText <$> maybePeek peekCString (#{ptr rodsObjStat_t, ownerZone} ptr)
    createTime <- toNum <$> maybePeek peekCString (#{ptr rodsObjStat_t, createTime} ptr)
    modifyTime <- toNum <$> maybePeek peekCString (#{ptr rodsObjStat_t, modifyTime} ptr)
    resourceHierarchy <- toText <$> maybePeek peekCString (#{ptr rodsObjStat_t, rescHier} ptr)
    return ObjectStat {..}

#include <irods/rodsGenQuery.h>

-- https://github.com/irods/irods/blob/master/lib/core/include/rodsGenQuery.h#L75
#{enum IntValue, Order
 , orderBy = ORDER_BY
 , orderByDesc = ORDER_BY_DESC
 }

-- https://github.com/irods/irods/blob/master/lib/core/include/rodsGenQuery.h#L93
#{enum IntValue, Operation
 , selectMin = SELECT_MIN
 , selectMax = SELECT_MAX
 , selectSum = SELECT_SUM
 , selectAvg = SELECT_AVG
 , selectCount = SELECT_COUNT
 }

-- https://github.com/irods/irods/blob/master/lib/core/include/rodsGenQuery.h#L84
#{enum Option, Option
 , returnTotalRowCount = RETURN_TOTAL_ROW_COUNT
 , noDistinct = NO_DISTINCT
 , quotaQuery = QUOTA_QUERY
 , autoClose = AUTO_CLOSE
 , upperCaseWhere = UPPER_CASE_WHERE
 }

-- R_ZONE_MAIN
-- https://github.com/irods/irods/blob/master/lib/core/include/rodsGenQuery.h#L121
#{enum Index, ZoneColumn
 , columnZoneId = COL_ZONE_ID
 , columnZoneName = COL_ZONE_NAME
 , columnZoneType = COL_ZONE_TYPE
 , columnZoneConnection = COL_ZONE_CONNECTION
 , columnZoneComment = COL_ZONE_COMMENT
 , columnZoneCreateTime = COL_ZONE_CREATE_TIME
 , columnZoneModifyTime = COL_ZONE_MODIFY_TIME
 }

-- R_USER_MAIN
-- https://github.com/irods/irods/blob/master/lib/core/include/rodsGenQuery.h#L130
#{enum Index, UserColumn
 , columnUserId = COL_USER_ID
 , columnUserName = COL_USER_NAME
 , columnUserType = COL_USER_TYPE
 , columnUserZone = COL_USER_ZONE
 , columnUserInfo = COL_USER_INFO
 , columnUserComment = COL_USER_COMMENT
 , columnUserCreateTime = COL_USER_CREATE_TIME
 , columnUserModifyTime = COL_USER_MODIFY_TIME
 , columnUserDnInvalid = COL_USER_DN_INVALID
 }

-- R_RESC_MAIN
-- https://github.com/irods/irods/blob/master/lib/core/include/rodsGenQuery.h#L142
#{enum Index, ResourceColumn
 , columnResourceId = COL_R_RESC_ID
 , columnResourceName = COL_R_RESC_NAME
 , columnResourceZoneName = COL_R_ZONE_NAME
 , columnResourceTypeName = COL_R_TYPE_NAME
 , columnResourceClassName = COL_R_CLASS_NAME
 , columnResourceLocation = COL_R_LOC
 , columnResourceVaultPath = COL_R_VAULT_PATH
 , columnResourceFreeSpace = COL_R_FREE_SPACE
 , columnResourceInfo = COL_R_RESC_INFO
 , columnResourceComment = COL_R_RESC_COMMENT
 , columnResourceCreateTime = COL_R_CREATE_TIME
 , columnResourceModifyTime = COL_R_MODIFY_TIME
 , columnResourceStatus = COL_R_RESC_STATUS
 , columnResourceFreeSpaceTime = COL_R_FREE_SPACE_TIME
 , columnResourceChildren = COL_R_RESC_CHILDREN
 , columnResourceContext = COL_R_RESC_CONTEXT
 , columnResourceParent = COL_R_RESC_PARENT
 , columnResourceParentContext = COL_R_RESC_PARENT_CONTEXT
 }

-- R_DATA_MAIN
-- https://github.com/irods/irods/blob/master/lib/core/include/rodsGenQuery.h#L162
#{enum Index, DataColumn
 , columnDataId = COL_D_DATA_ID
 , columnDataCollId = COL_D_COLL_ID
 , columnDataName = COL_DATA_NAME
 , columnDataReplNum = COL_DATA_REPL_NUM
 , columnDataVersion = COL_DATA_VERSION
 , columnDataTypeName = COL_DATA_TYPE_NAME
 , columnDataSize = COL_DATA_SIZE
 , columnDataRescName = COL_D_RESC_NAME
 , columnDataPath = COL_D_DATA_PATH
 , columnDataOwnerName = COL_D_OWNER_NAME
 , columnDataOwnerZone = COL_D_OWNER_ZONE
 , columnDataReplicaStatus = COL_D_REPL_STATUS
 , columnDataStatus = COL_D_DATA_STATUS
 , columnDataChecksum = COL_D_DATA_CHECKSUM
 , columnDataExpiry = COL_D_EXPIRY
 , columnDataMapId = COL_D_MAP_ID
 , columnDataComments = COL_D_COMMENTS
 , columnDataCreateTime = COL_D_CREATE_TIME
 , columnDataModifyTime = COL_D_MODIFY_TIME
 , columnDataMode = COL_DATA_MODE
 , columnDataRescHier = COL_D_RESC_HIER
 , columnDataRescId = COL_D_RESC_ID
 }

-- R_COLL_MAIN
-- https://github.com/irods/irods/blob/master/lib/core/include/rodsGenQuery.h#L187
#{enum Index, CollectionColumn
 , columnCollectionId = COL_COLL_ID
 , columnCollectionName = COL_COLL_NAME
 , columnCollectionParentName = COL_COLL_PARENT_NAME
 , columnCollectionOwnerName = COL_COLL_OWNER_NAME
 , columnCollectionOwnerZone = COL_COLL_OWNER_ZONE
 , columnCollectionMapId = COL_COLL_MAP_ID
 , columnCollectionInheritance = COL_COLL_INHERITANCE
 , columnCollectionComments = COL_COLL_COMMENTS
 , columnCollectionCreateTime = COL_COLL_CREATE_TIME
 , columnCollectionModifyTime = COL_COLL_MODIFY_TIME
 , columnCollectionType = COL_COLL_TYPE
 , columnCollectionInfo1 = COL_COLL_INFO1
 , columnCollectionInfo2 = COL_COLL_INFO2
 }

-- R_META_MAIN
-- https://github.com/irods/irods/blob/master/lib/core/include/rodsGenQuery.h#L202
#{enum Index, MetadataColumn
 , columnMetadataDataAttributeName = COL_META_DATA_ATTR_NAME
 , columnMetadataDataAttributeValue = COL_META_DATA_ATTR_VALUE
 , columnMetadataDataAttributeUnits = COL_META_DATA_ATTR_UNITS
 , columnMetadataDataAttributeId = COL_META_DATA_ATTR_ID
 , columnMetadataDataCreateTime = COL_META_DATA_CREATE_TIME
 , columnMetadataDataModifyTime = COL_META_DATA_MODIFY_TIME
 , columnMetadataCollectionAttributeName = COL_META_COLL_ATTR_NAME
 , columnMetadataCollectionAttributeValue = COL_META_COLL_ATTR_VALUE
 , columnMetadataCollectionAttributeUnits = COL_META_COLL_ATTR_UNITS
 , columnMetadataCollectionAttributeId = COL_META_COLL_ATTR_ID
 , columnMetadataCollectionCreateTime = COL_META_COLL_CREATE_TIME
 , columnMetadataCollectionModifyTime = COL_META_COLL_MODIFY_TIME
 , columnMetadataNamespaceColl = COL_META_NAMESPACE_COLL
 , columnMetadataNamespaceData = COL_META_NAMESPACE_DATA
 , columnMetadataNamespaceResc = COL_META_NAMESPACE_RESC
 , columnMetadataNamespaceUser = COL_META_NAMESPACE_USER
 , columnMetadataNamespaceRescGroup = COL_META_NAMESPACE_RESC_GROUP
 , columnMetadataNamespaceRule = COL_META_NAMESPACE_RULE
 , columnMetadataNamespaceMsrvc = COL_META_NAMESPACE_MSRVC
 , columnMetadataNamespaceMet2 = COL_META_NAMESPACE_MET2
 , columnMetadataResourceAttributeName = COL_META_RESC_ATTR_NAME
 , columnMetadataResourceAttributeValue = COL_META_RESC_ATTR_VALUE
 , columnMetadataResourceAttributeUnits = COL_META_RESC_ATTR_UNITS
 , columnMetadataResourceAttributeId = COL_META_RESC_ATTR_ID
 , columnMetadataResourceCreateTime = COL_META_RESC_CREATE_TIME
 , columnMetadataResourceModifyTime = COL_META_RESC_MODIFY_TIME
 , columnMetadataUserAttributeName = COL_META_USER_ATTR_NAME
 , columnMetadataUserAttributeValue = COL_META_USER_ATTR_VALUE
 , columnMetadataUserAttributeUnits = COL_META_USER_ATTR_UNITS
 , columnMetadataUserAttributeId = COL_META_USER_ATTR_ID
 , columnMetadataUserCreateTime = COL_META_USER_CREATE_TIME
 , columnMetadataUserModifyTime = COL_META_USER_MODIFY_TIME
 , columnMetadataResourceGroupAttributeName = COL_META_RESC_GROUP_ATTR_NAME
 , columnMetadataResourceGroupAttributeValue = COL_META_RESC_GROUP_ATTR_VALUE
 , columnMetadataResourceGroupAttributeUnits = COL_META_RESC_GROUP_ATTR_UNITS
 , columnMetadataResourceGroupAttributeId = COL_META_RESC_GROUP_ATTR_ID
 , columnMetadataResourceGroupCreateTime = COL_META_RESC_GROUP_CREATE_TIME
 , columnMetadataResourceGroupModifyTime = COL_META_RESC_GROUP_MODIFY_TIME
 , columnMetadataRuleAttributeName = COL_META_RULE_ATTR_NAME
 , columnMetadataRuleAttributeValue = COL_META_RULE_ATTR_VALUE
 , columnMetadataRuleAttributeUnits = COL_META_RULE_ATTR_UNITS
 , columnMetadataRuleAttributeId = COL_META_RULE_ATTR_ID
 , columnMetadataRuleCreateTime = COL_META_RULE_CREATE_TIME
 , columnMetadataRuleModifyTime = COL_META_RULE_MODIFY_TIME
 , columnMetadataMsrvcAttributeName = COL_META_MSRVC_ATTR_NAME
 , columnMetadataMsrvcAttributeValue = COL_META_MSRVC_ATTR_VALUE
 , columnMetadataMsrvcAttributeUnits = COL_META_MSRVC_ATTR_UNITS
 , columnMetadataMsrvcAttributeId = COL_META_MSRVC_ATTR_ID
 , columnMetadataMsrvcCreateTime = COL_META_MSRVC_CREATE_TIME
 , columnMetadataMsrvcModifyTime = COL_META_MSRVC_MODIFY_TIME
 , columnMetadataMet2AttributeName = COL_META_MET2_ATTR_NAME
 , columnMetadataMet2AttributeValue = COL_META_MET2_ATTR_VALUE
 , columnMetadataMet2AttributeUnits = COL_META_MET2_ATTR_UNITS
 , columnMetadataMet2AttributeId = COL_META_MET2_ATTR_ID
 , columnMetadataMet2CreateTime = COL_META_MET2_CREATE_TIME
 , columnMetadataMet2ModifyTime = COL_META_MET2_MODIFY_TIME
 }
