{-# LANGUAGE CApiFFI #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Irods where

import Control.Monad (void)
import qualified Data.ByteString as BS
import Data.ByteString.Builder.Extra (defaultChunkSize)
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Unsafe as BSU (unsafeUseAsCStringLen, unsafePackMallocCString)
import Data.Coerce (coerce)
import Data.Text (Text, pack, unpack)
import Foreign hiding (void)
import Foreign.C.String
import Foreign.C.Types
import GHC.Generics (Generic)
import Prelude
import System.IO.Unsafe
import Web.HttpApiData (FromHttpApiData)

import IrodsC

newtype ErrorCode =
  ErrorCode Int

newtype Connection =
  Connection (Ptr ())

newtype Host =
  Host Text
  deriving stock (Generic)
  deriving newtype (FromHttpApiData)

newtype Port =
  Port Int

newtype UserName =
  UserName Text
  deriving stock (Eq, Generic, Ord, Show)

newtype Zone =
  Zone Text
  deriving stock (Generic)
  deriving newtype (FromHttpApiData)

newtype Flags =
  Flags Int

newtype Password =
  Password Text
  deriving stock (Eq, Generic, Ord, Show)

newtype FileDescriptor =
  FileDescriptor CInt
  deriving stock (Show)

newtype CollectionHandle =
  CollectionHandle CInt
  deriving stock (Show)

data CollectionResult
  = CollectionEmpty
  | CollectionExists
  | CollectionIsObject
  | CollectionNotEmpty
  | CollectionNotFound
  | CollectionOK
  | CollectionUnhandled
  deriving stock (Show)

data ConnectionResult
  = ConnectionOK
  | ConnectionInvalidUser
  | ConnectionInvalidAuthentication
  | ConnectionUnhandled

data FileError
  = FileInvalidPath
  | FileInternalNullInput
  | FileOverwriteWithoutForce
  | FileUnhandled

-- C function imports

{-
-- void init_client_api_table(void)
foreign import capi unsafe "init_client_api_table"
  c_init_client_api_table :: IO ()
-}

-- https://github.com/irods/irods/blob/master/lib/core/include/rodsLog.h
-- const char *rodsErrorName(int errorValue, char **subName)
foreign import capi unsafe "irods/rodsLog.h rodsErrorName"
  c_rodsErrorName :: CInt -> Ptr CString -> CString

-- https://github.com/irods/irods/blob/master/lib/core/include/rcConnect.h
-- rcComm_t * rcConnect(const char *rodsHost, int rodsPort, const char *userName, const char *rodsZone, int reconnFlag, rErrMsg_t *errMsg)
foreign import capi safe "irods/rcConnect.h rcConnect"
  c_rcConnect :: CString -> CInt -> CString -> CString -> CInt -> Ptr ErrorMsg -> IO Connection

-- https://github.com/irods/irods/blob/master/lib/core/include/rcConnect.h
-- int rcDisconnect(rcComm_t *conn)
foreign import capi safe "irods/rcConnect.h rcDisconnect"
  c_rcDisconnect :: Connection -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/core/include/rcConnect.h
-- int clientLoginWithPassword(rcComm_t *conn, char* password)
foreign import capi safe "irods/rcConnect.h clientLoginWithPassword"
  c_clientLoginWithPassword :: Connection -> CString -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/dataObjCreate.h
-- int rcDataObjCreate(rcComm_t *conn, dataObjInp_t *dataObjInp)
foreign import capi safe "irods/dataObjCreate.h rcDataObjCreate"
  c_rcDataObjCreate :: Connection -> Ptr DataObject -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/dataObjUnlink.h
-- int rcDataObjUnlink(rcComm_t *conn, dataObjInp_t *dataObjUnlinkInp)
foreign import capi safe "irods/dataObjUnlink.h rcDataObjUnlink"
  c_rcDataObjUnlink :: Connection -> Ptr DataObject -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/dataObjOpen.h
-- int rcDataObjOpen(rcComm_t *conn, dataObjInp_t *dataObjInp)
foreign import capi safe "irods/dataObjOpen.h rcDataObjOpen"
  c_rcDataObjOpen :: Connection -> Ptr DataObject -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/dataObjWrite.h
-- int rcDataObjWrite(rcComm_t *conn, openedDataObjInp_t *dataObjWriteInp, bytesBuf_t *dataObjWriteInpBBuf)
foreign import capi safe "irods/dataObjWrite.h rcDataObjWrite"
  c_rcDataObjWrite :: Connection -> Ptr OpenObject -> Ptr BytesBuffer -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/dataObjClose.h
-- int rcDataObjClose(rcComm_t *conn, openedDataObjInp_t *dataObjCloseInp)
foreign import capi safe "irods/dataObjClose.h rcDataObjClose"
  c_rcDataObjClose :: Connection -> Ptr OpenObject -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/dataObjRead.h
-- int rcDataObjRead(rcComm_t *conn, openedDataObjInp_t *dataObjReadInp, bytesBuf_t *dataObjReadOutBBuf)
foreign import capi safe "irods/dataObjRead.h rcDataObjRead"
  c_rcDataObjRead :: Connection -> Ptr OpenObject -> Ptr BytesBuffer -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/dataObjLock.h
-- int rcDataObjLock(rcComm_t *conn, dataObjInp_t *dataObjInp)
foreign import capi safe "irods/dataObjLock.h rcDataObjLock"
  c_rcDataObjLock :: Connection -> Ptr DataObject -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/dataObjLock.h
-- int rcDataObjUnlock(rcComm_t *conn, dataObjInp_t *dataObjInp)
foreign import capi safe "irods/dataObjLock.h rcDataObjUnlock"
  c_rcDataObjUnlock :: Connection -> Ptr DataObject -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/dataObjChksum.h
-- int rcDataObjChksum(rcComm_t *conn, dataObjInp_t *dataObjChksumInp, char **outChksum)
foreign import capi safe "irods/dataObjChksum.h rcDataObjChksum"
  c_rcDataObjChksum :: Connection -> Ptr DataObject -> Ptr CString -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/genQuery.h
-- int rcGenQuery(rcComm_t *conn, genQueryInp_t *genQueryInp, genQueryOut_t **genQueryOut)
foreign import capi safe "irods/genQuery.h rcGenQuery"
  c_rcGenQuery :: Connection -> Ptr QueryInput -> Ptr (Ptr QueryOutput) -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/collCreate.h
-- int rcCollCreate(rcComm_t *conn, collInp_t *collCreateInp)
foreign import capi safe "irods/collCreate.h rcCollCreate"
  c_rcCollCreate :: Connection -> Ptr CollectionInput -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/rmColl.h
-- int rcRmColl(rcComm_t *conn, collInp_t *rmCollInp, int vFlag)
foreign import capi safe "irods/rmColl.h rcRmColl"
  c_rcRmColl :: Connection -> Ptr CollectionInput -> CInt -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/modColl.h
-- int rcModColl(rcComm_t *conn, collInp_t *modCollInp)
foreign import capi safe "irods/modColl.h rcModColl"
  c_rcModColl :: Connection -> Ptr CollectionInput -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/openCollection.h
-- int rcOpenCollection(rcComm_t *conn, collInp_t *openCollInp)
foreign import capi safe "irods/openCollection.h rcOpenCollection"
  c_rcOpenCollection :: Connection -> Ptr CollectionInput -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/closeCollection.h
-- int rcCloseCollection(rcComm_t *conn, int handleInxInp)
foreign import capi safe "irods/closeCollection.h rcCloseCollection"
  c_rcCloseCollection :: Connection -> CInt -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/readCollection.h
-- int rcReadCollection(rcComm_t *conn, int handleInxInp, collEnt_t **collEnt)
foreign import capi safe "irods/readCollection.h rcReadCollection"
  c_rcReadCollection :: Connection -> CInt -> Ptr (Ptr CollectionEntry) -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/api/include/pamAuthRequest.h
-- int rcPamAuthRequest(rcComm_t *conn, pamAuthRequestInp_t *pamAuthRequestInp, pamAuthRequestOut_t **pamAuthRequestOut)
foreign import capi safe "irods/pamAuthRequest.h rcPamAuthRequest"
  c_rcPamAuthRequest :: Connection -> Ptr PamRequest -> Ptr (Ptr PamResponse) -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/core/include/miscUtil.h#L233
-- int freeCollEnt(collEnt_t *collEnt)
foreign import capi unsafe "irods/miscUtil.h freeCollEnt"
  c_freeCollEnt :: Ptr CollectionEntry -> IO CInt

-- https://github.com/irods/irods/blob/master/lib/core/include/rcMisc.h#L114
-- int freeGenQueryOut(genQueryOut_t **genQueryOut)
foreign import capi unsafe "irods/rcMisc.h freeGenQueryOut"
  c_freeGenQueryOut :: Ptr (Ptr QueryOutput) -> IO CInt

-- Haskell functions

irodsErrorName :: ErrorCode -> (Text, Text)
irodsErrorName (ErrorCode val) = unsafePerformIO $ alloca $ \ptr -> do
  name <- peekCString $ c_rodsErrorName (fromIntegral val) ptr
  subname <- peekCString =<< peek ptr
  return (pack name, pack subname)

rcConnect :: Host -> Port -> UserName -> Zone -> Flags -> IO (Either ErrorMsg Connection)
rcConnect (Host host) (Port port) (UserName user) (Zone zone) (Flags flags') =
  withCString (unpack host) $ \hostC ->
    withCString (unpack user) $ \userC ->
      withCString (unpack zone) $ \zoneC ->
        alloca $ \errorMsg -> do
          con <-
            c_rcConnect
              hostC
              (fromIntegral port)
              userC
              zoneC
              (fromIntegral flags')
              errorMsg
          if (coerce con) /= nullPtr
          then do
            putStrLn "Valid connection encountered"
            return $ Right con
          else do
            putStrLn "Null pointer connection encountered"
            if errorMsg == nullPtr
            then do
              putStrLn "Null error message pointer encountered"
              return $ Left $ ErrorMsg 0 (pack "Null pointer")
            else do
              putStrLn "Valid error message pointer encountered"
              err <- peek errorMsg
              return $ Left err

rcDisconnect :: Connection -> IO CInt
rcDisconnect connection = do
  putStrLn "Disconnecting"
  c_rcDisconnect connection

clientLoginWithPassword :: Connection -> Password -> IO ConnectionResult
clientLoginWithPassword con (Password pass) =
  withCString (unpack pass) $ \passC -> do
    res <- c_clientLoginWithPassword con passC
    case res of
      (-827000) -> do
        putStrLn "Invalid user (username or zone)"
        return ConnectionInvalidUser
      (-826000) -> do
        putStrLn "Invalid authentication (password)"
        return ConnectionInvalidAuthentication
      0 -> return ConnectionOK
      _ -> return ConnectionUnhandled

rcDataObjCreate :: Connection -> DataObject -> IO (Either FileError FileDescriptor)
rcDataObjCreate con obj = with obj $ \obj_p -> do
  putStrLn "Creating object"
  fd <- c_rcDataObjCreate con obj_p
  print fd
  return $ case fd of
    (-24000) -> Left FileInternalNullInput
    (-312000) -> Left FileOverwriteWithoutForce
    (-808000) -> Left FileInvalidPath
    x | x > 0 -> Right $ FileDescriptor fd
      | x == 0 -> Left FileInvalidPath
      | otherwise -> Left FileUnhandled

rcDataObjUnlink :: Connection -> DataObject -> IO (Either FileError FileDescriptor)
rcDataObjUnlink con obj = with obj $ \obj_p -> do
  putStrLn "Unlinking object"
  fd <- c_rcDataObjUnlink con obj_p
  print fd
  return $ case fd of
    (-808000) -> Left FileInvalidPath
    0 -> Right $ FileDescriptor fd
    _ -> Left FileUnhandled

rcDataObjOpen :: Connection -> DataObject -> IO (Either FileError FileDescriptor)
rcDataObjOpen con obj = with obj $ \obj_p -> do
  putStrLn "Opening object"
  fd <- c_rcDataObjOpen con obj_p
  print fd
  return $ case fd of
    (-808000) -> Left FileInvalidPath
    x | x > 0 -> Right $ FileDescriptor fd
      | x == 0 -> Left FileInvalidPath
      | otherwise -> Left FileUnhandled

rcDataObjWriteStrict :: Connection -> OpenObject -> BS.ByteString -> IO CInt
rcDataObjWriteStrict con obj byteString =
  with obj { len = (fromIntegral . BS.length) byteString } $ \obj_p ->
    BSU.unsafeUseAsCStringLen byteString $ \c_str ->
      with (BytesBuffer c_str) $ \buf_p ->
        c_rcDataObjWrite con obj_p buf_p

rcDataObjWriteLazy :: Connection -> FileDescriptor -> BL.ByteString -> IO ()
rcDataObjWriteLazy connection (FileDescriptor fd) = writeLoop
  where
    object :: OpenObject
    object = OpenObject {l1descInx = fd, len = 0, whence = seekSet, conditions = KeywordValuePairs []}
    chunkSize :: Int64
    chunkSize = fromIntegral $ 32 * defaultChunkSize
    writeLoop :: BL.ByteString -> IO ()
    writeLoop bl = do
      let (chunk, rest) = BL.splitAt chunkSize bl
      rcDataObjWriteStrict connection object (BL.toStrict chunk)
      if BL.null rest then return () else writeLoop rest

rcDataObjReadChunk :: Connection -> OpenObject -> IO BS.ByteString
rcDataObjReadChunk connection object = with object { len = fromIntegral chunkSize } $ \obj_p ->
  allocaBytes chunkSize $ \bytes_p ->
    with (BytesBuffer (bytes_p, chunkSize)) $ \buf_p -> do
      c_rcDataObjRead connection obj_p buf_p
      BytesBuffer buf <- peek buf_p
      BS.packCStringLen buf
  where
    chunkSize :: Int
    chunkSize = 32 * defaultChunkSize

rcDataObjReadLazy :: Connection -> FileDescriptor -> IO BL.ByteString
rcDataObjReadLazy connection (FileDescriptor fd) = BL.fromChunks <$> readLoop
 where
  object :: OpenObject
  object = OpenObject {l1descInx = fd, len = 0, whence = seekSet, conditions = KeywordValuePairs []}
  readLoop :: IO [BS.ByteString]
  readLoop = do
    chunk <- rcDataObjReadChunk connection object
    loop <- if BS.null chunk then return [] else unsafeInterleaveIO readLoop
    return (chunk : loop)

rcDataObjClose :: Connection -> FileDescriptor -> IO CInt
rcDataObjClose connection (FileDescriptor fd) = do
  putStrLn "Closing object"
  with object $ \obj_p -> c_rcDataObjClose connection obj_p
  where
    object :: OpenObject
    object = OpenObject {l1descInx = fd, len = 0, whence = seekSet, conditions = KeywordValuePairs []}

rcDataObjLock :: Connection -> DataObject -> IO CInt
rcDataObjLock connection object = with object $ \obj_p -> c_rcDataObjLock connection obj_p

rcDataObjUnlock :: Connection -> DataObject -> IO CInt
rcDataObjUnlock connection object = with object $ \obj_p -> c_rcDataObjUnlock connection obj_p

rcDataObjChksum :: Connection -> DataObject -> IO (Maybe BS.ByteString)
rcDataObjChksum connection object =
  with object $ \obj_p ->
    alloca $ \ptr -> do
      res <- c_rcDataObjChksum connection obj_p ptr
      case res of
        0 -> do
          p <- peek ptr
          s <- maybePeek BSU.unsafePackMallocCString p
          return s
        _ -> return Nothing

rcGenQuery :: Connection -> QueryInput -> IO (Maybe QueryOutput)
rcGenQuery connection query =
  with query $ \query_p ->
    alloca $ \ptr -> do
      ret <- c_rcGenQuery connection query_p ptr
      case ret of
        0 -> do
          p <- peek ptr
          mres <- maybePeek peek p
          case mres of
            Nothing -> return Nothing
            Just res -> do
              void $ c_freeGenQueryOut ptr
              if attributeCount res < 1 then
                return Nothing
              else
                return $ Just res
        _ -> return Nothing

rcCollCreate :: Connection -> CollectionInput -> IO CollectionResult
rcCollCreate connection collection = do
  putStrLn "Creating collection"
  with collection $ \collection_p -> do
    res <- c_rcCollCreate connection collection_p
    return $ case res of
      (-809000) -> CollectionExists
      (-814000) -> CollectionNotFound
      (-834000) -> CollectionIsObject
      0 -> CollectionOK
      _ -> CollectionUnhandled

rcRmColl :: Connection -> CollectionInput -> IO CollectionResult
rcRmColl connection collection = do
  putStrLn "Removing collection"
  with collection $ \collection_p -> do
    res <- c_rcRmColl connection collection_p 0
    return $ case res of
      (-808000) -> CollectionEmpty
      (-814000) -> CollectionNotFound
      (-821000) -> CollectionNotEmpty
      0 -> CollectionOK
      _ -> CollectionUnhandled

rcModColl :: Connection -> CollectionInput -> IO ()
rcModColl connection collection = do
  putStrLn "Modifying collection"
  with collection $ \collection_p ->
    print =<< c_rcModColl connection collection_p

rcOpenCollection :: Connection -> CollectionInput -> IO (Either CollectionResult CollectionHandle)
rcOpenCollection connection collection = do
  putStrLn "Opening collection"
  with collection $ \collection_p -> do
    ch <- c_rcOpenCollection connection collection_p
    print ch
    return $ case ch of
      (-310000) -> Left CollectionNotFound
      (-834000) -> Left CollectionIsObject
      x | x >= 0 -> Right $ CollectionHandle ch
        | otherwise -> Left CollectionUnhandled

rcCloseCollection :: Connection -> CollectionHandle -> IO ()
rcCloseCollection connection (CollectionHandle ch) = do
  putStrLn "Closing collection"
  print =<< c_rcCloseCollection connection ch

rcReadCollection :: Connection -> CollectionHandle -> IO (Either CollectionResult [CollectionEntry])
rcReadCollection connection (CollectionHandle ch) = do
  putStrLn "Reading collection"
  readLoop
  where
    readLoop :: IO (Either CollectionResult [CollectionEntry])
    readLoop = do
      c <- readColl
      case c of
        Left x -> return $ Left x
        Right x -> do
          l <- readLoop
          return $ case l of
            Right y -> Right (x : y)
            Left CollectionEmpty -> Right (x : [])
            Left y -> Left y
    readColl :: IO (Either CollectionResult CollectionEntry)
    readColl =
      alloca $ \ptr -> do
        res <- c_rcReadCollection connection ch ptr
        print res
        case res of
          (-808000) -> return $ Left CollectionEmpty
          0 -> do
            p <- peek ptr
            me <- maybePeek peek p
            case me of
              Nothing -> return $ Left CollectionUnhandled
              Just e -> do
                void $ c_freeCollEnt p
                return $ Right $ e
          _ -> return $ Left CollectionUnhandled

rcPamAuthRequest :: Connection -> PamRequest -> IO (Maybe PamResponse)
rcPamAuthRequest connection request = do
  putStrLn "Starting PAM authentication"
  with request $ \request_p ->
    alloca $ \ptr -> do
      res <- c_rcPamAuthRequest connection request_p ptr
      case res of
        0 -> do
          p <- peek ptr
          e <- peek p
          return $ Just e
        _ -> return Nothing
