{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import ClassyPrelude
import qualified Data.ByteString.Lazy as BL

import Irods
import IrodsC

main :: IO ()
main = do
  let iRodsPath = "/"
      iRodsColl1 = iRodsPath ++ "myCollection1"
      iRodsColl2 = iRodsPath ++ "myCollection2"
      iRodsFile = iRodsPath ++ "hello-world"
      localFile = "random"
  byteFile <- BL.readFile localFile
  fileSize <- withBinaryFile localFile ReadMode hFileSize
  -- Connect
  con <-
    rcConnect
      (Host "")
      (Port 1247)
      (UserName "")
      (Zone "")
      (Flags 0)
  case con of
    Left e -> do
      print e
      case status e of
        (-305111) -> putStrLn "Connection refused"
    Right c -> do
      -- Login
      l <- clientLoginWithPassword c (Password "")
      case l of
        ConnectionInvalidUser -> return ()
        ConnectionInvalidAuthentication -> return ()
        ConnectionOK -> do
          -- {-
          -- Get PID
          qr <-
            rcGenQuery
              c
              QueryInput
                { maxRows = 5
                , continueIndex = 0
                , rowOffset = 0
                , options = []
                , conditions = KeywordValuePairs []
                , select = IndexIntValuePairs [(columnMetadataDataAttributeValue, orderBy)]
                , sqlConditions = IndexValuePairs [(columnMetadataDataAttributeName, "='PID'")]
                }
          print qr
          -- Create collection
          cc <-
            rcCollCreate
              c
              CollectionInput
                { collectionName = iRodsColl1
                , flags = []
                , conditions = KeywordValuePairs []
                }
          -- Open collection
          cr <-
            rcOpenCollection
              c
              CollectionInput
                { collectionName = iRodsColl2
                , flags = []
                , conditions = KeywordValuePairs []
                }
          case cr of
            Left e -> return ()
            Right ch -> do
              -- Read collection
              print =<< rcReadCollection c ch
              -- Close collection
              print =<< rcCloseCollection c ch
          -- Remove collection
          rc <-
            rcRmColl
              c
              CollectionInput
                { collectionName = iRodsColl1
                , flags = []
                , conditions = KeywordValuePairs []
                }
          -- Unlink object
          fr <-
            rcDataObjUnlink
              c
              DataObject
                { objectPath = iRodsFile
                , createMode = 0
                , openFlags = readWrite
                , dataSize = 0
                , numThreads = 16
                , conditions = KeywordValuePairs []
                }
          case fr of
            Left FileInvalidPath -> do
              putStrLn "Unlink: Invalid path"
              return ()
            Right fd -> print fd
          -- Create object
          fr <-
            rcDataObjCreate
              c
              DataObject
                { objectPath = iRodsFile
                , createMode = 0o655
                , openFlags = writeOnly
                , dataSize = fromIntegral fileSize
                , numThreads = 16
                , conditions = KeywordValuePairs [
                    -- (destinationResourceNameKW, "disk"),
                    (registerChecksumKW, "")
                  ]
                }
          case fr of
            Left FileInvalidPath -> do
              putStrLn "Create: Invalid path"
              return ()
            Left FileInternalNullInput -> do
              putStrLn "Create: Internal null input"
              return ()
            Right fd -> do
              print fd
              -- Write and close object
              print =<< rcDataObjWriteLazy c fd byteFile
              print =<< rcDataObjClose c fd
          -- -}
          -- {-
          -- Open object
          fr <-
            rcDataObjOpen
              c
              DataObject
                { objectPath = iRodsFile
                , createMode = 0
                , openFlags = readOnly
                , dataSize = 0
                , numThreads = 16
                , conditions = KeywordValuePairs []
                }
          case fr of
            Left FileInvalidPath -> do
              putStrLn "Open: Invalid path"
              return ()
            Right fd -> do
              print fd
              -- Read and close object
              lbs <- rcDataObjReadLazy c fd
              BL.writeFile "random2" $ lbs  -- BL.take 10 lbs
              print =<< rcDataObjClose c fd
              -- -}
      -- Disconnect
      print =<< rcDisconnect c
